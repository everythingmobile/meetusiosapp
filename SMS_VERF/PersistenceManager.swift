//
//  PersistenceManager.swift
//  SMS_VERF
//
//  Created by Pradeep on 20/09/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation

class PersistenceManager{
    class var sharedPersistenceManager : PersistenceManager{
        
    struct Static{
        static var instance : PersistenceManager?
        static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token){
            Static.instance = PersistenceManager()
        
        }
        return Static.instance!
    }
    
    
    func persistUserInfo( userInfo : UserInfo){
        let nsUserDefaults = NSUserDefaults.standardUserDefaults()
        nsUserDefaults.setObject(userInfo.userName,forKey: "userName")
        nsUserDefaults.setObject(userInfo.userPhoneNumber,forKey: "userPhoneNumber")
        nsUserDefaults.synchronize()
        
    }
    
    func persistAuthToken(token : String){
        let nsUserDefaults = NSUserDefaults.standardUserDefaults()
        nsUserDefaults.setObject(token, forKey: "authToken")
        nsUserDefaults.synchronize()
    }
    
    func getAuthToken()->  String?{
        let nsUserDefaults = NSUserDefaults.standardUserDefaults()
        var authToken: String? = nsUserDefaults.objectForKey("authToken") as String?
        return authToken
        
    }
}
