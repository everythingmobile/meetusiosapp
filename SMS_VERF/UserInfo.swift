//
//  UserInfo.swift
//  SMS_VERF
//
//  Created by sudheer kumar meesala on 9/7/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class UserInfo {
    var userPhoneNumber : Int
    var userName : String
    var emailId : String
    var registered : Bool
    init(userName : String, userPhoneNumber : Int,emailId : String = "",registered : Bool = false){
        self.userName = userName
        self.userPhoneNumber = userPhoneNumber
        self.emailId = emailId
        self.registered = registered
        
        
    }

    required init() {
        self.userName = ""
        self.userPhoneNumber = 0
        self.emailId = ""
        self.registered = false
        
    }


   
    
    func toDict()->Dictionary<String,AnyObject>{
        var dict =  Dictionary<String,AnyObject>()
        dict["userPhoneNumber"] = self.userPhoneNumber
        dict["userName"] = self.userName
        dict["emailId"] = self.emailId
        dict["registered"] = self.registered
        return dict
        
    }
    
}
