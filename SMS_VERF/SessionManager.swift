//
//  SessionManager.swift
//  SMS_VERF
//
//  Created by sudheer kumar meesala on 9/7/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import CoreTelephony
class SessionManager{
    var baseUrl = "http://ec2-54-179-132-4.ap-southeast-1.compute.amazonaws.com:8080/MeetusServer/"
    class var sharedsessionManager : SessionManager{
        
    struct Static{
        static var instance : SessionManager?
        static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token){
            Static.instance = SessionManager()
            Static.instance!.userSession = UserSession()
        }
        return Static.instance!
    }
    
    internal var userSession :  UserSession?
    
    
    
    func registerUser(userInfo : UserInfo){
        
        self.userSession?.userInfo = userInfo
        request(.POST, "http://ec2-54-179-132-4.ap-southeast-1.compute.amazonaws.com:8080/MeetusServer/useradd", parameters:  userInfo.toDict(), encoding: .URL)
            .responseString {(request, response, string, error) in
                if  error != nil{
                    println(error)
                }else{
                    PersistenceManager.sharedPersistenceManager.persistUserInfo(userInfo)
                }
        }
    }
    
    func  getCountryCode ()->String{
        var telephoneNetworkInfo = CTTelephonyNetworkInfo()
        var carrier = telephoneNetworkInfo.subscriberCellularProvider
        
        println(carrier?.mobileCountryCode?)
        return (carrier?.mobileCountryCode != nil) ? carrier.mobileCountryCode : ""
        
        
    }
    
    func getUserFeed(userMeetUpsRequest : UserMeetUpsRequest) -> Request {
        return request(.GET, baseUrl+"getUserFeed", parameters:userMeetUpsRequest.toDict())
        
    }
    func createMeetUpFeed(fromArray arrayOfFeed : NSArray) -> Array<MeetUpEntity>{
        var meetUpFeedList = Array<MeetUpEntity>()
        for feedObject in arrayOfFeed{
            if let feed = feedObject as? NSDictionary{
                var meetUpEntity = MeetUpEntity()
                meetUpEntity.destinationLatitude =  feed["destinationLatitude"] as String
                meetUpEntity.destinationLongitude = feed["destinationLongitude"] as String
                meetUpEntity.meetUpId = feed["meetUpId"] as String
                
                if let friendsList = feed["friends"] as? NSArray{
                    meetUpEntity.friends = self.createUserList(fromArray: friendsList)
                }
                if let participantList = feed["participants"] as? NSArray{
                    meetUpEntity.participants = self.createUserList(fromArray: participantList)
                    
                }
                meetUpFeedList.append(meetUpEntity)
            }
        }
        return meetUpFeedList
    }
    
    func createUserList(fromArray arr : NSArray) -> Array<UserInfo>{
        var userList = Array<UserInfo>()
        for item in arr{
            if let dict = item as? NSDictionary{
                var user = self.createUserInfo(fromDictionary: dict)
                userList.append(user)
            }
            
        }
        return userList
    }
    
    func createUserInfo(fromDictionary dict : NSDictionary)-> UserInfo{
        var user = UserInfo()
        user.userPhoneNumber = dict["userPhoneNumber"] as Int
        user.userName =  dict["userName"] as String
        user.emailId = dict["emailId"] as String
        user.registered = dict["registered"] as Bool
        return user
    }
}