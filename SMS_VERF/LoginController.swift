//
//  LoginController.swift
//  SMS_VERF
//
//  Created by sudheer kumar meesala on 9/7/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import UIKit
class LoginController : UIViewController,UITextFieldDelegate {
    @IBOutlet weak var phoneNumberTF: UITextField!
    
   
    @IBOutlet weak var userNameTF: UITextField!
    
    
    @IBOutlet weak var registerButton: UIButton!
    @IBAction func editingChanged(sender : AnyObject){
        var phoneNumber = self.phoneNumberTF.text
        var userName = self.userNameTF.text;
        if phoneNumber.isEmpty == false && userName.isEmpty == false{
            self.registerButton.enabled = true
        }
        else{
            self.registerButton.enabled = false
        }
        
    }
    
    @IBAction func registerUser(sender: AnyObject) {
        let sessionManager = SessionManager.sharedsessionManager
        var code = sessionManager.getCountryCode()
        code = (code == "") ? "91" : code
        var phoneNumber : Int = ( code  + self.phoneNumberTF.text).toInt()!
        var userName : String = self.userNameTF.text;
        var userInfo = UserInfo(userName: userName,userPhoneNumber: phoneNumber )
        sessionManager.registerUser(userInfo)
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerButton.enabled = false
               
        
        
        
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    func textFieldShouldReturn(textField : UITextField! )-> Bool{
        self.view.endEditing(true)
        return true
    }
    
    override func touchesBegan(touches: NSSet!, withEvent event: UIEvent!){
        self.view.endEditing(true)
    }
    
    
     func textField(textField : UITextField!, shouldChangeCharactersInRange range : NSRange,
        replacementString string : NSString!)-> Bool{
            if(self.phoneNumberTF == textField){
                if string.length == 0{
                    return true
                }
            var myCharSet = NSCharacterSet.decimalDigitCharacterSet()
                for var i = 0;i<string.length;i++ {
                    var s = string.characterAtIndex(i)
                    if myCharSet.characterIsMember(s){
                        return true
                    }
                }
                return false
            }
            return true;
    }
    
}
