//
//  MeetUpEntity.swift
//  SMS_VERF
//
//  Created by Pradeep on 20/09/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class MeetUpEntity   {
    var  destinationLatitude : String = ""
    var  destinationLongitude : String = ""
    var  meetUpId : String = ""
    var friends : Array<UserInfo> = Array<UserInfo>()
    var participants : Array<UserInfo> = Array<UserInfo>()
    
    func toDict()->Dictionary<String,AnyObject>{
        var dict =  Dictionary<String,AnyObject>()
        
        
        dict["destinationLatitude"] = self.destinationLatitude
        dict["destinationLongitude"] = self.destinationLongitude
        dict["meetUpId"] = self.meetUpId
        dict["friends"] = self.friends
        dict["participants"] = self.participants
        
        return dict
        
    }
    
    
    
}
