//
//  ViewController.swift
//  SMS_VERF
//
//  Created by sudheer kumar meesala on 8/24/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {


    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var verificationField: UITextField!
    
    var verificationManager  = VerificationManger()
    @IBAction func editingChanged(sender : AnyObject){
        var phoneNumber = self.verificationField.text
        if phoneNumber.isEmpty == true {
            self.verifyButton.enabled = false
        }else{
            self.verifyButton.enabled = true
        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.verifyButton.enabled = false
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func verifyButton(sender : AnyObject) {
        self.verificationField.resignFirstResponder()
        var verification =  Verification()
        verification.phoneNumber = SessionManager.sharedsessionManager.userSession!.userInfo!.userPhoneNumber
        verification.verificationString = self.verificationField.text;
        self.verifyButton.enabled = false
        verificationManager.verifyUser(verification).responseString { [unowned self](req, res, string, error) -> Void in
            if error == nil{
                if let authToken = string  {
                    if authToken != ""{
                        SessionManager.sharedsessionManager.userSession!.authToken = authToken
                        PersistenceManager.sharedPersistenceManager.persistAuthToken(authToken)
                        println("verified successfully")
                        println(PersistenceManager.sharedPersistenceManager.getAuthToken()!)
                    }else{
                        println("cant verify")
                    }
                }else{
                    println("cant verify")
                }
            }else{
                println(error)
            }
            
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                self.verifyButton.enabled = true
            })
            
        }
        
        
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldShouldReturn(textField : UITextField! )-> Bool{
        self.verificationField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: NSSet!, withEvent event: UIEvent!){
        self.view.endEditing(true)
    }


}

