//
//  FeedViewController.swift
//  SMS_VERF
//
//  Created by Pradeep on 20/09/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
import UIKit
class FeedViewController : UITableViewController,UITableViewDelegate,UITableViewDataSource{
    var numbers = [1,2,3,4,5]
    var meetUpFeed = Array<MeetUpEntity>()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadFeed()
    }
    
    func loadFeed(){
        let sessionManager = SessionManager.sharedsessionManager
        var userMeetReq = UserMeetUpsRequest(userPhoneNumber: 917406444421, pageNum: "1")
        println("gete")
        var request = sessionManager.getUserFeed(userMeetReq)
        request.responseJSON { (request, response, data, error) -> Void in
            if (error == nil) {
                if let arrayOfFeed = data as? NSArray{
                    var feed = sessionManager.createMeetUpFeed(fromArray: arrayOfFeed)
                    self.meetUpFeed = feed
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        self.tableView.reloadData()
                    })
                }
            }
        }
    }
    override func numberOfSectionsInTableView(tableView:UITableView!)->Int{
        return 1
    }
    
    override func tableView(tableView:UITableView!,numberOfRowsInSection section:Int)->Int{
        return meetUpFeed.count
    }
    override func tableView(tableView:UITableView!,cellForRowAtIndexPath indexPath:NSIndexPath!)->UITableViewCell{
        
        
        var cell : UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("Cell") as? UITableViewCell
        if cell==nil{
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        }
        //cell!.textLabel.lineBreakMode = NSLineBreakByWordWrapping
        cell!.textLabel.numberOfLines = 0
        cell!.textLabel.text = self.makeFeedDescription(meetUpFeed[indexPath.row])
        
        //will work only after including api key
        var req = request(.GET,"http://maps.google.com/maps/api/staticmap?center=40.714728,-73.998672&zoom=12&size=265x65&maptype=roadmap&sensor=true&markers=color:red|40.714728,-73.998672")
        req.response { (request, response, data, error) -> Void in
            if let imgData = data as? NSData{
                var image = UIImage( data : imgData)
                NSOperationQueue.mainQueue().addOperationWithBlock{
                    () -> Void in
                    cell!.imageView.image = image
                }
            }
            
            
        }
        
        
        return cell!
    }
    override func tableView(tableView:UITableView!,didSelectRowAtIndexPath indexPath:NSIndexPath!){
        
    }
    
    func makeFeedDescription(meetUpEntity : MeetUpEntity)-> String{
        var feedText = String()
        if(meetUpEntity.friends.count == 1){
            feedText = feedText + meetUpEntity.friends[0].userName + "is going to meetup"
        }
        else if(meetUpEntity.friends.count == 2){
            feedText = feedText + " \(meetUpEntity.friends[0].userName), \(meetUpEntity.friends[1].userName)"
                + " are meeting up"
        }
        else{
            feedText = feedText + " \(meetUpEntity.friends[0].userName), \(meetUpEntity.friends[1].userName)"
            + " and \(meetUpEntity.friends.count-2) others are meeting up"
        }
        return feedText
    }
}
