//
//  UserMeetUpsRequest.swift
//  SMS_VERF
//
//  Created by Pradeep on 20/09/14.
//  Copyright (c) 2014 pradeep. All rights reserved.
//

import Foundation
class UserMeetUpsRequest{
    var  userPhoneNumber : Int
    var pageNum : String
    
    init(userPhoneNumber : Int,pageNum : String){
        self.userPhoneNumber = userPhoneNumber
        self.pageNum = pageNum
    }
    
    func toDict()->Dictionary<String,AnyObject>{
        var dict =  Dictionary<String,AnyObject>()
        
    
        dict["pageNum"] = self.pageNum
        dict["userPhoneNumber"] = self.userPhoneNumber
        
        return dict
        
    }
}